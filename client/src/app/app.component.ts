import { Component, ViewChild } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MatButton } from '@angular/material/button';
import { UploadService } from './services/upload.service';

export enum StatusIndicator {
  PREPARING = 'PREPARING',
  UPLOADING = 'UPLOADING'
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // Button - Upload
  @ViewChild('btnUpload', { static: true}) btnUpload: MatButton;
  // Form
  form: FormGroup = new FormGroup({
    nameFile: new FormControl({ value: '', disabled: true }, Validators.required)
  });
  // File in FormData
  formData: FormData = new FormData();
  // File selected
  file: any;
  // Status indicator
  statusIndicator: StatusIndicator = null;
  // Uploading progress
  uploadingProgress: number = 0;

  // Constructor
  constructor( private uploadService: UploadService ) { }

  /**
   * @description Get file
   * @param {event} event - Input's event
   */
  loadFile(event): void {
    // Disabled upload button
    this.btnUpload.disabled = true;
    // Show status indicator 'PREPARING'
    this.statusIndicator = StatusIndicator.PREPARING;

    console.time('preparing')
    // FileReader
    let reader = new FileReader();
    // Select file
    let file = event.target.files[0];
    // Set file's name to form
    this.form.get('nameFile').setValue(file.name);
    // Select file
    this.file = event.target['files'];
    // Render file
    reader.readAsArrayBuffer(file);
    
    reader.onloadend = () => { 
      // Enable upload file
      this.btnUpload.disabled = false;
      // Remove status indicator
      this.statusIndicator = null;
      console.timeEnd('preparing')
    }
  }

  /** @description Upload file */
  upload(): void {
    console.log('UPLOAD')
    console.log(this.file)

    // Remove old files
    this.formData.delete('file');

    // Set FormData
    for(let i=0; i< this.file.length; i++) this.formData.append('file', this.file[i])

    // Show status indicator 'UPLOADING'
    this.statusIndicator = StatusIndicator.UPLOADING;
    // Set Uploading progress to 0
    this.uploadingProgress = 0;
    // Disable upload button
    this.btnUpload.disabled = true;
    
    this.uploadService.upload(this.formData)
      .subscribe(val => {
        // Show uploading progress
        if(val && val['progress']) this.uploadingProgress = val['progress']
        // Remove status indicator if updeted
        if(val && (val['status'] == 'done' || val['status'] == 'error')) this.statusIndicator = null;
        // Enable upload button if there's an error
        if(val && val['status'] == 'error')this.btnUpload.disabled = false;
      })
  }
}
