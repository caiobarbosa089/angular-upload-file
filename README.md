# angular-upload-file
This project aims to exemplify a file upload using the **Angular 9** and **NodeJS**.

## Requirements

Be sure you have **node**,  **npm** and **yarn** installed, you can download easily in the [official website](https://nodejs.org/en/download/).

To install **yarn** follow the steps in the [official website](https://classic.yarnpkg.com/en/docs/install).

After the installation you can open the terminal and test both node and npm.

- Run `node -v`to see node version.
- Run `npm -v` to see npm version.
- Run `yarn -v`to see yarn version.

If you have all messages you are ready to go! :)

The **npm** and **yarn** are used to install and manage project dependencies and run it by command line.

## How to use 
### Server
- Run `yarn install` to install dependencies.
- Run `yarn start`to start the server.

### Client
Enter the client folder

- Run `npm install` to install dependencies.
- Run `ng server`to start the server.


The project will run in `http://localhost:4200`
